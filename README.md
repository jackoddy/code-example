# DOM Manipulation

This code comes from a larger project that was a proof of concept for adding accessibility features to existing HTML through a chrome extension. It uses the built in TreeWalker implementation to walk the dom, selecting relevant nodes based on a predicate function and then injecting a style based on another function.

I see this code as clean because I feel it is concise and expressive, to the point that I recieved praise from a colleague after leaving the project when he came to the code and had at extend the functionality - it was a simple as adding an aditional predicate function to filter different nodes, and adding a new function to select and change the desired style as neccessary.

The example I have included is the `fontSize` feature. Here inside `changeTextSize.js` the function to select a style is created and also a function to compute the new style. The style to change (the first string value) is what will be selected and the function it is given will be what is used to compute the new value.

Here `changeSize` is a curried function where the multiplier can be set and then returns a function that takes a value and returns a string. The value the returned function takes is the current value of a node, whereas what is returned is what will replace it. Currying this functionality allows `changeSize` to be reusable and to be used declaritively by the developer.

inside `decreaseText.run.js` is where the code is executed to manipulate the dom. It imports a function called `changeDom` which takes two arguments. The first is the function that replaces the css value of a node and the second is a filtering function - providing an expressive way to declare what will be maniuplated. So it can almost be read a `changeDom(how, where)`.
