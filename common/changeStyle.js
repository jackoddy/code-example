export const getValue = (property, node) => (
  window
  .getComputedStyle(node, null)
  .getPropertyValue(property)
);


export const changeStyle = (property, cb) => (
  (node) => {
    const value = getValue(property, node);
    node.style[property] = cb(value);
  }
);
