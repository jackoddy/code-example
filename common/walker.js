export const walk = (f, Walker) => {
  if (Walker.nextNode()) {
    f(Walker.currentNode);
    walk(f, Walker);
  }
};

export const treeWalker = condition => (
  document.createTreeWalker(
    document.body,
    NodeFilter.SHOW_ELEMENT,
    { acceptNode: condition }
  )
);

export default (f, condition) => {
  walk(f, treeWalker(condition));
};

