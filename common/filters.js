import { isContrastLow }from './contrast.js'

export const isTextNode = node => node.innerText ? NodeFilter.FILTER_ACCEPT : NodeFilter.FILTER_SKIP;

export const isLabelNode = node => (node.inputTag === 'LABEL') ? NodeFilter.FILTER_ACCEPT : NodeFilter.FILTER_SKIP;

export const isInputNode = node => (node.inputTag === 'INPUT') ? NodeFilter.FILTER_ACCEPT : NodeFilter.FILTER_SKIP;

export const isLowContrastNode = node => ( isContrastLow(node) && node.innerText ) ? NodeFilter.FILTER_ACCEPT : NodeFilter.FILTER_SKIP;
