import { changeStyle } from '../../common/changeStyle';

const changeSize = multiplyer => value => `${parseFloat(value) * multiplyer}px`;

export default multiplyer => changeStyle('font-size', changeSize(multiplyer));
