import changeSize from './changeTextSize';
import { isTextNode } from '../../common/filters';
import changeDom from '../../common/walker';

changeDom(changeSize(0.95), isTextNode);
